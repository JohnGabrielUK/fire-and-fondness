extends CanvasLayer

onready var button_camera_shake = $Center/VBox/HBox/Grid/Button_CameraShake
onready var button_shader = $Center/VBox/HBox/Grid/Button_Shader
onready var button_scanlines = $Center/VBox/HBox/Grid/Button_Scanlines
onready var button_screen_warp = $Center/VBox/HBox/Grid/Button_ScreenWarp
onready var button_glow = $Center/VBox/HBox/Grid/Button_Glow
onready var button_transition = $Center/VBox/HBox/Grid/Button_Transition
onready var button_back = $Center/VBox/BackButton

onready var overlay = $Overlay

func _ready():
	var use_shader = Global.get_setting("use_shader")
	button_camera_shake.pressed = Global.get_setting("camera_shake")
	button_shader.pressed = use_shader
	button_scanlines.pressed = Global.get_setting("scanlines")
	button_screen_warp.pressed = Global.get_setting("screen_warp")
	button_glow.pressed = Global.get_setting("glow")
	button_transition.pressed = Global.get_setting("transition")
	# If shader not being used, disable shader options
	button_scanlines.disabled = !use_shader
	button_screen_warp.disabled = !use_shader
	button_glow.disabled = !use_shader
	button_back.grab_focus()

func set_camera_shake(button_pressed):
	Global.set_setting("camera_shake", button_pressed)

func set_shader(button_pressed):
	Global.set_setting("use_shader", button_pressed)
	overlay.set_shader(button_pressed)
	button_scanlines.disabled = !button_pressed
	button_screen_warp.disabled = !button_pressed
	button_glow.disabled = !button_pressed

func set_scanlines(button_pressed):
	Global.set_setting("scanlines", button_pressed)
	overlay.mat.set_shader_param("scanlines", button_pressed)

func set_screen_warp(button_pressed):
	Global.set_setting("screen_warp", button_pressed)
	overlay.mat.set_shader_param("screen_warp", button_pressed)

func set_glow(button_pressed):
	Global.set_setting("glow", button_pressed)
	overlay.mat.set_shader_param("glow", button_pressed)

func set_transition(button_pressed):
	Global.set_setting("transition", button_pressed)
	overlay.mat.set_shader_param("transition", button_pressed)

func back():
	Global.save_game(Global.game_progress)
	get_tree().change_scene("res://scenes/TitleScreen.tscn")
